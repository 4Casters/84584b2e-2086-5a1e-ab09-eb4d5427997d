import logging


def init(context):
    logging.debug("init")
    # register the data you are interested in
    context.ccy_pair = "EURUSD"
    context.freq = "1M"
    context.size = 100
    context.start_date = '1-05-2018'
    context.end_date = '30-05-2018'


# df is a numpy 2d array
def on_quote(context):
    logging.debug("quote")
    df = context.data
    size = context.size
    last_close = df['close'][size-1]
    last_ccy_pair = context.last_ccy_pair
    index = context.index
    trade_api = context.tradeapi
    if index % 100 == 0:
        if context.hasPosition:
            ret = trade_api.trade(last_ccy_pair, -2, last_close)
            context.hasPosition = False
        else:
            ret = trade_api.trade(last_ccy_pair, 2, last_close)
            context.hasPosition = True


def on_results(context, resp):
    t = resp['type']
    if t == "RESULTS":
        logging.debug("result")
