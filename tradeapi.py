import json


class TradeApi:

    def __init__(self, session, origin):
        self.url_order = origin+'/actions/order/{}/{}/{}'
        self.session = session

    def trade(self, ccypair, qty, price):     #price tolerance, timeout, etc
        url = self.url_order.format(ccypair, str(qty), str(price))
        self.session.request('GET', url.strip('\n'))
        ret = self.session.getresponse().read().decode()
        return json.loads(ret)
